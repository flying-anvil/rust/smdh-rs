# SMDH RS

Tool and library for parsing SMDH files (3DS icons/application metadata) and their embedded icons.

## Usage

```
Usage: smdh-rs <COMMAND>

Commands:
  info     Print the meta data stored in the given file
  convert  Convert the icon to a common format
  help     Print this message or the help of the given subcommand(s)
```

### Info

```
smdh-rs info [OPTIONS] <INPUT>

Arguments:
  <INPUT>  Path to the SMDH file

Options:
  -f, --format <FORMAT>  Format of the output [possible values: text, json, debug]
  -r, --region <REGION>  Region to use for locale data (name, ratings)
```

### Convert

```
smdh-rs convert [OPTIONS] <INPUT> [OUTPUT]

Arguments:
  <INPUT>   Path to the SMDH file
  [OUTPUT]  Path to the output file. Format is inferred from extension. If omitted, $input.png is used. (example: icon.png)

Options:
      --size <SIZE>  Which icon to convert (small, large) [possible values: small, large]
```

### Extract

You can prefix the above commands with `extract` (e.g. `extract info`, `extract convert`)
to extract the SMDH file from a decrypted 3DS ROM (.3ds).

You can also save an embedded SMDH file from a ROM using `smdh-rs extract save path/to/rom.3ds path/to/output/icon.smdh`.

### Examples

```
smdh-rs info ./input/icon.smdh
smdh-rs info ./input/icon.smdh --region germany
smdh-rs info ./input/icon.smdh --format text

smdh-rs convert ./input/icon.smdh ~/Images/smash.png --size large
smdh-rs convert ./input/smash.smdh ~/Images --size large

smdh-rs extract save ./input/rom.3ds ./rom.smdh
```

## File Specification

https://www.3dbrew.org/wiki/SMDH
