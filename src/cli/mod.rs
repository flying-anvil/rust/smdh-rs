use std::{process::exit};

use clap::{Parser, Subcommand};
use self::{info_command::InfoCommand, convert_command::ConvertCommand, extract_command::ExtractCommand};

mod info_command;
mod convert_command;
mod extract_command;

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
#[command(author, version, about, long_about = None)]
#[command(disable_colored_help = false)]
#[command(
    // help_template = "{author-with-newline} {about-section}Version: {version} \n {usage-heading} {usage} \n {all-args} {tab}"
    help_template = "{usage-heading} {usage} \n\n{all-args}{tab}\n\n\x1B[1;4mAbout:\x1B[0m\n\x1B[3m  Authors: {author-with-newline}  Version: {version}\x1B[0m"
)]
struct Cli {
    /// What to do with a given SMDH file
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Clone, Debug)]
enum Command {
    /// Print the meta data stored in the given file
    Info(InfoCommand),

    /// Convert the icon to a common format
    Convert(ConvertCommand),

    /// Extract the SMDH file form a decrypted ROM file
    Extract(ExtractCommand),

    #[cfg(debug_assertions)]
    Debug,
}

pub(crate) fn run() {
    let args = Cli::parse();

    #[cfg(debug_assertions)]
    println!("{args:#?}");

    let result = match args.command {
        Command::Info(command) => command.run(),
        Command::Convert(command) => command.run(),
        Command::Extract(command) => command.run(),

        #[cfg(debug_assertions)]
        Command::Debug => debug(),
    };

    if let Err(error) = result {
        eprintln!("{error}");
        exit(1);
    }
}

#[cfg(debug_assertions)]
use smdh_rs::prelude::{Result, Error};

#[cfg(debug_assertions)]
fn debug() -> Result<()> {
    Err(Error::Generic("Nothing here".to_string()))
}
