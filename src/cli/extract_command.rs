use clap::{Args, Subcommand};
use smdh_rs::{prelude::{Result, Error}, rom_extractor};

use super::{info_command::InfoCommand, convert_command::ConvertCommand};

#[derive(Args, Clone, Debug)]
pub struct ExtractCommand {
    /// What to wot with the SMDH of a ROM file
    #[command(subcommand)]
    subcommand: SubCommand,
}

#[derive(Subcommand, Clone, Debug)]
pub enum SubCommand {
    Save(SaveCommand),
    Info(InfoCommand),
    Convert(ConvertCommand),
}

#[derive(Args, Clone, Debug)]
pub struct SaveCommand {
    /// Path to the ROM file
    input: String,

    /// Path to the output file
    output: String,
}

impl ExtractCommand {
    pub fn run(&self) -> Result<()> {
        match &self.subcommand {
            SubCommand::Save(command) => command.run(),
            SubCommand::Info(command) => command.run_rom_extract(),
            SubCommand::Convert(command) => command.run_rom_extract(),
        }
    }
}

impl SaveCommand {
    fn run(&self) -> Result<()> {
        let bytes = rom_extractor::extract_smdh_bytes(&self.input)?;

        std::fs::write(&self.output, bytes)
            .map_err(|error| Error::Generic(format!(
                "Could not write extracted SMDH to {}: {}",
                self.output,
                error,
            )))?;

        Ok(())
    }
}
