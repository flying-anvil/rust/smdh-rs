use std::{path::{PathBuf, Path}, str::FromStr};

use clap::{Args, ValueEnum};
use smdh_rs::{prelude::{Result, Error}, SMDH, rom_extractor};


#[derive(Args, Clone, Debug)]
pub struct ConvertCommand {
    /// Path to the SMDH file
    input: String,

    /// Path to the output file. Format is inferred from extension. If omitted, $input.png is used. (example: icon.png)
    output: Option<String>,

    /// Which icon to convert (small, large)
    #[arg(long)]
    size: Option<IconSize>,
}


#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum IconSize {
    Small,
    #[default]
    Large,
}

impl ConvertCommand {
    pub fn run(&self) -> Result<()> {
        let parsed = SMDH::try_from_file(&self.input)?;
        self.write_icon(parsed)
    }

    pub fn run_rom_extract(&self) -> Result<()> {
        let bytes = rom_extractor::extract_smdh_bytes(&self.input)?;
        let parsed = SMDH::try_from_bytes(&bytes)?;
        self.write_icon(parsed)
    }

    fn write_icon(&self, parsed: SMDH) -> Result<()> {
        let buffer = match self.size.unwrap_or_default() {
            IconSize::Small => parsed.icon_graphics.parse_small_icon(),
            IconSize::Large => parsed.icon_graphics.parse_large_icon(),
        };

        let output = build_output_file_path(self)?;
        buffer.save(output).map_err(|error| Error::Generic(format!("{error}")))?;

        Ok(())
    }
}

fn build_output_file_path(command: &ConvertCommand) -> Result<String> {
    let input_path = &PathBuf::from_str(&command.input).map_err(|error| Error::Generic(format!("{error}")))?;
    let mut output = command.output.clone().unwrap_or_else(|| {
        input_path.with_extension("png").to_str().unwrap().to_string()
    });

    if output.starts_with('~') {
        output = shellexpand::tilde(&output).to_string();
    }

    if Path::new(&output).is_dir() {
        let file_stem = input_path.file_stem().unwrap();
        output = format!("{}/{}.png", output.trim_end_matches('/'), file_stem.to_str().unwrap());
    }

    Ok(output)
}
