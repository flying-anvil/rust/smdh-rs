use clap::{Args, ValueEnum};
use smdh_rs::{prelude::{Result, Error}, SMDH, RegionSpecificGameRating, rom_extractor};

#[derive(Args, Clone, Debug)]
pub struct InfoCommand {
    /// Path to the SMDH file
    input: String,

    /// Format of the output
    #[arg(value_enum, short, long)]
    format: Option<InfoFormat>,

    /// Region to use for locale data (name, ratings)
    #[arg(value_enum, short, long, value_parser = region_parser)]
    region: Option<InfoRegion>,
}

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum InfoFormat {
    #[default]
    Text,
    Json,
    Debug,
}

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum InfoRegion {
    Japan,
    #[default]
    English,
    France,
    German,
    Italy,
    Spain,
    ChinaSimplified,
    Korea,
    Dutch,
    Portugal,
    Russia,
    ChinaTraditional,
    Australia,
}

fn region_parser(raw: &str) -> Result<InfoRegion> {
    let cleaned = raw.chars().filter(|c| !c.is_whitespace()).collect::<String>()
        .replace('-', "")
        .to_lowercase();

    match cleaned.as_ref() {
        "japan" => Ok(InfoRegion::Japan),
        "jp" => Ok(InfoRegion::Japan),

        "english" => Ok(InfoRegion::English),
        "en" => Ok(InfoRegion::English),

        "france" => Ok(InfoRegion::France),
        "fr" => Ok(InfoRegion::France),

        "german" => Ok(InfoRegion::German),
        "ger" => Ok(InfoRegion::German),
        "de" => Ok(InfoRegion::German),

        "italy" => Ok(InfoRegion::Italy),
        "it" => Ok(InfoRegion::Italy),

        "spain" => Ok(InfoRegion::Spain),
        "sp" => Ok(InfoRegion::Spain),

        "china" => Ok(InfoRegion::ChinaSimplified),
        "chinasimplified" => Ok(InfoRegion::ChinaSimplified),
        "chinas" => Ok(InfoRegion::ChinaSimplified),
        "chinatraditional" => Ok(InfoRegion::ChinaTraditional),
        "chinat" => Ok(InfoRegion::ChinaTraditional),

        "korea" => Ok(InfoRegion::Korea),
        "kr" => Ok(InfoRegion::Korea),

        "dutch" => Ok(InfoRegion::Dutch),
        "netherlands" => Ok(InfoRegion::Dutch),
        "netherland" => Ok(InfoRegion::Dutch),
        "nl" => Ok(InfoRegion::Dutch),

        "portugal" => Ok(InfoRegion::Portugal),
        "pt" => Ok(InfoRegion::Portugal),

        "russia" => Ok(InfoRegion::Russia),
        "ru" => Ok(InfoRegion::Russia),

        "australia" => Ok(InfoRegion::Australia),
        "au" => Ok(InfoRegion::Australia),

        _ => Err(Error::Generic("Unknown region".to_string()))
    }
}

impl InfoCommand {
    pub fn run(&self) -> Result<()> {
        let parsed = SMDH::try_from_file(&self.input)?;
        self.print_output(parsed);

        Ok(())
    }

    pub fn run_rom_extract(&self) -> Result<()> {
        let bytes = rom_extractor::extract_smdh_bytes(&self.input)?;
        let parsed = SMDH::try_from_bytes(&bytes)?;
        self.print_output(parsed);

        Ok(())
    }

    fn print_output(&self, parsed: SMDH) {
        match self.format.unwrap_or_default() {
            InfoFormat::Text => print_info_text(parsed, &self.region.unwrap_or_default()),
            InfoFormat::Json => todo!("Formatting as JSON is not yet implemented"),
            InfoFormat::Debug => println!("{parsed:#?}"),
        }
    }
}

fn print_info_text(parsed: SMDH, region: &InfoRegion) {
    let titles = parsed.application_titles;
    let title = match region {
        InfoRegion::Japan => titles.japanese(),
        InfoRegion::English => titles.english(),
        InfoRegion::France => titles.french(),
        InfoRegion::German => titles.german(),
        InfoRegion::Italy => titles.italian(),
        InfoRegion::Spain => titles.spanish(),
        InfoRegion::ChinaSimplified => titles.chinese_simplified(),
        InfoRegion::Korea => titles.korean(),
        InfoRegion::Dutch => titles.dutch(),
        InfoRegion::Portugal => titles.portuguese(),
        InfoRegion::Russia => titles.russian(),
        InfoRegion::ChinaTraditional => titles.chinese_traditional(),
        InfoRegion::Australia => titles.english(),
    };

    let unavailable = RegionSpecificGameRating::unavailable();
    let age_restrictions = &parsed.application_settings.region_specific_game_ratings;
    let age_restriction = match region {
        InfoRegion::Japan => age_restrictions.cero(),
        InfoRegion::English => age_restrictions.esrb(),
        InfoRegion::France => age_restrictions.pegi_gen(),
        InfoRegion::German => age_restrictions.usk(),
        InfoRegion::Italy => age_restrictions.pegi_gen(),
        InfoRegion::Spain => age_restrictions.pegi_gen(),
        InfoRegion::ChinaSimplified => &unavailable,
        InfoRegion::Korea => age_restrictions.grb(),
        InfoRegion::Dutch => age_restrictions.pegi_gen(),
        InfoRegion::Portugal => age_restrictions.pegi_prt(),
        InfoRegion::Russia => &unavailable,
        InfoRegion::ChinaTraditional => &unavailable,
        InfoRegion::Australia => age_restrictions.cob(),
    };

    println!("\x1B[1;4m{}\x1B[0m", title.short_description_string());
    println!("{}", title.long_description_string());
    println!();
    println!("Regions: {}", parsed.application_settings.region_lockout.to_string_list().join(", "));
    println!("EULA Version: {}", parsed.application_settings.eula_version);
    println!("Age restriction: {}", age_restriction);
}
