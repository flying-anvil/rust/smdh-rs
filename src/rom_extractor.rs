use std::{io::Read, path::Path};

use crate::{prelude::{Error, Result}, SMDH};

pub fn extract_smdh_bytes(file: impl AsRef<Path>) -> Result<[u8; SMDH::FILE_SIZE]> {
    let file = std::fs::File::open(file).map_err(|error| Error::Generic(format!("{error}")))?;
    let mut file = std::io::BufReader::new(file);

    let mut chunk = vec![0u8; 0x100];
    // SMDH should be within the first 10 MiB.
    for i in (0..10485760).step_by(0x100) {
        file.read_exact(&mut chunk).unwrap();

        if chunk.starts_with(&SMDH::MAGIC) {
            println!("Found SMDH header at offset 0x{i:X} ({i})");

            let remaining = SMDH::FILE_SIZE - chunk.len();
            let mut remaining_buffer = vec![0u8; remaining];
            file.read_exact(&mut remaining_buffer).unwrap();

            chunk.append(&mut remaining_buffer);
            let mut bytes = [0u8; SMDH::FILE_SIZE];

            chunk.into_iter().enumerate().for_each(|(index, byte)| bytes[index] = byte);

            return Ok(bytes);
        }
    }

    Err(Error::Generic("Could not find SMDH header in ROM file".to_string()))
}
