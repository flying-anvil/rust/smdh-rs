use std::io::{Cursor, Read};

use byteorder::{ReadBytesExt, LE};
use image::{ImageBuffer, Pixel};

pub struct IconGraphics {
    small: [u16; 0x240],
    large: [u16; 0x900],
}

impl IconGraphics {
    pub(super) fn read_from_positioned_curser(reader: &mut Cursor<&[u8]>) -> Self {
        let mut data = Self::default();

        for raw_pixel in data.small.iter_mut() {
            *raw_pixel = reader.read_u16::<LE>().unwrap();
        }

        for raw_pixel in data.large.iter_mut() {
            *raw_pixel = reader.read_u16::<LE>().unwrap();
        }

        // reader.read_exact(&mut data.small);
        // reader.read_exact(&mut data.large);

        data
    }
}

impl IconGraphics {
    // TODO: Where do these offsets come from? Can they be programmatically inferred?
    const OFFSETS: [u8; 64] = [
         0,  1,  8,  9,  2,  3, 10, 11,
        16, 17, 24, 25, 18, 19, 26, 27,
         4,  5, 12, 13,  6,  7, 14, 15,
        20, 21, 28, 29, 22, 23, 30, 31,
        32, 33, 40, 41, 34, 35, 42, 43,
        48, 49, 56, 57, 50, 51, 58, 59,
        36, 37, 44, 45, 38, 39, 46, 47,
        52, 53, 60, 61, 54, 55, 62, 63,
    ];

    pub fn parse_small_icon(&self) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
        self.parse_icon(&self.small, 24)
    }

    pub fn parse_large_icon(&self) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
        self.parse_icon(&self.large, 48)
    }

    fn parse_icon(&self, data: &[u16], size: u8) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
        // let size = (data.len() / 24) as u8;
        let mut buffer = ImageBuffer::new(size as u32, size as u32);

        let mut i = 0;
        for tile_y in (0..size).step_by(8) {
            for tile_x in (0..size).step_by(8) {
                for k in 0..64 {
                    let offset_x = Self::OFFSETS[k] & 0b0111;
                    let offset_y = Self::OFFSETS[k] >> 3;

                    let raw_color = data[i];
                    i += 1;

                    let r = ((raw_color & 0b1111_1000_0000_0000) >> 8) as u8;
                    let g = ((raw_color & 0b0000_0111_1110_0000) >> 3) as u8;
                    let b = ((raw_color & 0b0000_0000_0001_1111) << 3) as u8;

                    buffer.put_pixel(
                        (tile_x + offset_x) as u32,
                        (tile_y + offset_y) as u32,
                        image::Rgb([r, g, b]),
                    );
                }
            }
        }

        buffer
    }
}

impl Default for IconGraphics {
    fn default() -> Self {
        Self {
            small: [0u16; 0x240],
            large: [0u16; 0x900],
        }
    }
}

impl std::fmt::Debug for IconGraphics {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("IconGraphics")
            .field("small", &"…")
            .field("large", &"…")
            .finish()
    }
}

#[cfg(test)]
mod test {
    #[allow(non_snake_case)]
    #[test]
    fn test_conversion_RGB565_RGB888() {
        let raw: u16 = 0x81FC;

        // println!("Raw:     {raw:b}");

        // let r = (((raw >> 11) & 0b0000_0000_0001_1111) << 3) as u8;
        // let g = (((raw >>  5) & 0b0000_0000_0011_1111) << 2) as u8;
        // let b = (((raw >>  0) & 0b0000_0000_0001_1111) << 3) as u8;

        // println!("Masked:  {:0>16b}", (raw & 0b1111_1000_0000_0000));
        // println!("Shifted:         {:0>8b}", ((raw & 0b1111_1000_0000_0000) >> 8) as u8);
        // println!("Shifted: {}", ((raw & 0b1111_1000_0000_0000) >> 8) as u8);

        let r = ((raw & 0b1111_1000_0000_0000) >> 8) as u8;
        let g = ((raw & 0b0000_0111_1110_0000) >> 3) as u8;
        let b = ((raw & 0b0000_0000_0001_1111) << 3) as u8;

        // println!("{:0>8b}", 128);
        // println!("{r:0>8b}");

        assert_eq!(128, r);
        assert_eq!(60, g);
        assert_eq!(224, b);
    }
}
