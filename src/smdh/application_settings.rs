use std::{fmt::{Debug, Display}, fs::read, io::Cursor};

use byteorder::{ReadBytesExt, LE, BE};

#[derive(Debug)]
#[readonly::make]
pub struct ApplicationSettings {
    pub region_specific_game_ratings: RegionSpecificGameRatings,
    pub region_lockout: RegionLockout,
    pub match_maker_id: MatchMakerId,
    pub flags: Flags,
    pub eula_version: EulaVersion,
    pub reserved: [u8; 2],
    pub default_frame: f32,
    pub street_pass_id: u32,
}

impl ApplicationSettings {
    pub(super) fn parse_from_positioned_curser(reader: &mut Cursor<&[u8]>) -> Self {
        let mut ratings = RegionSpecificGameRatings::default();

        for rating in ratings.iter_mut() {
            rating.0 = reader.read_u8().unwrap();
        }

        let region_lockout = RegionLockout(reader.read_u32::<LE>().unwrap());
        let match_maker_id = MatchMakerId {
            match_maker_id: reader.read_u32::<LE>().unwrap(),
            match_maker_bit_id: reader.read_u64::<LE>().unwrap(),
        };

        let flags = Flags(reader.read_u32::<LE>().unwrap());
        let eula_version = EulaVersion {
            minor: reader.read_u8().unwrap(),
            major: reader.read_u8().unwrap(),
        };

        let reserved = reader.read_u16::<LE>().unwrap().to_le_bytes();
        let default_frame = reader.read_f32::<LE>().unwrap();
        let street_pass_id = reader.read_u32::<LE>().unwrap();

        Self {
            region_specific_game_ratings: ratings,
            region_lockout,
            match_maker_id,
            flags,
            eula_version,
            reserved,
            default_frame,
            street_pass_id,
        }
    }
}

#[derive(Default, Debug)]
#[readonly::make]
pub struct RegionSpecificGameRatings ([RegionSpecificGameRating; 16]);

#[derive(Default)]
pub struct RegionSpecificGameRating (u8);

#[derive(Debug)]
#[readonly::make]
pub struct RegionLockout (u32);

#[derive(Debug)]
#[readonly::make]
pub struct MatchMakerId {
    pub match_maker_id: u32,
    pub match_maker_bit_id: u64,
}

#[readonly::make]
pub struct Flags (u32);

#[derive(Debug)]
#[readonly::make]
pub struct EulaVersion {
    pub minor: u8,
    pub major: u8,
}

impl RegionSpecificGameRating {
    pub fn unavailable() -> Self {
        Self(0)
    }

    pub fn active(&self) -> bool { self.0 & 0x80 > 0 }
    pub fn rating_pending(&self) -> bool { self.0 & 0x40 > 0 }
    pub fn age_restriction(&self) -> u8 {
        if !self.active() {
            return 255;
        }

        if self.0 & 0x20 > 0 {
            return 0;
        }

        self.0 - 0x80
    }
}

impl RegionSpecificGameRatings {
    pub fn cero(&self) -> &RegionSpecificGameRating { &self.0[0] }
    pub fn esrb(&self) -> &RegionSpecificGameRating { &self.0[1] }
    pub fn reserved_1(&self) -> &RegionSpecificGameRating { &self.0[2] }
    pub fn usk(&self) -> &RegionSpecificGameRating { &self.0[3] }
    pub fn pegi_gen(&self) -> &RegionSpecificGameRating { &self.0[4] }
    pub fn reserved_2(&self) -> &RegionSpecificGameRating { &self.0[5] }
    pub fn pegi_prt(&self) -> &RegionSpecificGameRating { &self.0[6] }
    pub fn pegi_bbfc(&self) -> &RegionSpecificGameRating { &self.0[7] }
    pub fn cob(&self) -> &RegionSpecificGameRating { &self.0[8] }
    pub fn grb(&self) -> &RegionSpecificGameRating { &self.0[9] }
    pub fn cgsrr(&self) -> &RegionSpecificGameRating { &self.0[10] }
    pub fn reserved_3(&self) -> &RegionSpecificGameRating { &self.0[11] }
    pub fn reserved_4(&self) -> &RegionSpecificGameRating { &self.0[12] }
    pub fn reserved_5(&self) -> &RegionSpecificGameRating { &self.0[13] }
    pub fn reserved_6(&self) -> &RegionSpecificGameRating { &self.0[14] }
    pub fn reserved_7(&self) -> &RegionSpecificGameRating { &self.0[15] }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<RegionSpecificGameRating> {
        self.0.iter_mut()
    }
}

impl Display for RegionSpecificGameRating {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if !self.active() {
            return write!(f, "N/A");
        }

        if self.rating_pending() {
            return write!(f, "pending");
        }

        let age = self.age_restriction();
        write!(f, "{}", age)
    }
}

impl Debug for RegionSpecificGameRating {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("RegionSpecificGameRating")
            .field("active", &self.active())
            .field("rating_pending", &self.rating_pending())
            .field("age_restriction", &self.age_restriction())
            .finish()
    }
}

impl RegionLockout {
    // These methods return if a region is allowed

    pub fn japan(&self) -> bool { self.0 & 0b0000_0001 > 0 }
    pub fn north_america(&self) -> bool { self.0 & 0b0000_0010 > 0 }
    pub fn europe(&self) -> bool { self.0 & 0b0000_0100 > 0 }
    pub fn australia(&self) -> bool { self.0 & 0b0000_1000 > 0 }
    pub fn china(&self) -> bool { self.0 & 0b0001_0000 > 0 }
    pub fn korea(&self) -> bool { self.0 & 0b0010_0000 > 0 }
    pub fn taiwan(&self) -> bool { self.0 & 0b0100_0000 > 0 }
    pub fn region_free(&self) -> bool { self.0 == 0x7FFFFFFF}

    pub fn to_string_list(&self) -> Vec<&str> {
        if self.region_free() {
            return vec!["Region Free"];
        }

        let mut list = Vec::new();

        if self.japan() {list.push("Japan")}
        if self.north_america() {list.push("North America")}
        if self.europe() {list.push("Europe")}
        if self.australia() {list.push("Australia")}
        if self.china() {list.push("China")}
        if self.korea() {list.push("Korea")}
        if self.taiwan() {list.push("Taiwan")}

        list
    }
}

impl Flags {
    pub fn visible(&self) -> bool { self.0 & 0b0000_0000_0001 > 0 }
    pub fn auto_boot(&self) -> bool { self.0 & 0b0000_0000_0010 > 0 }

    #[allow(non_snake_case)]
    pub fn allow_3D(&self) -> bool { self.0 & 0b0000_0000_0100 > 0 }
    pub fn requires_eula(&self) -> bool { self.0 & 0b0000_0000_1000 > 0 }

    pub fn autosave_on_exit(&self) -> bool { self.0 & 0b0000_0001_0000 > 0 }
    pub fn uses_extended_banner(&self) -> bool { self.0 & 0b0000_0010_0000 > 0 }
    pub fn requires_region_specific_game_rating(&self) -> bool { self.0 & 0b0000_0100_0000 > 0 }
    pub fn uses_save_data(&self) -> bool { self.0 & 0b0000_1000_0000 > 0 }

    pub fn records_usage(&self) -> bool { self.0 & 0b0001_0000_0000 > 0 }
    pub fn disables_save_backups(&self) -> bool { self.0 & 0b0100_0000_0000 > 0 }

    #[allow(non_snake_case)]
    pub fn new_3DS_exclusive(&self) -> bool { self.0 & 0b1000_0000_0000 > 0 }
}

impl Debug for Flags {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Flags")
            .field("visible", &self.visible())
            .field("auto_boot", &self.auto_boot())
            .field("allow_3D", &self.allow_3D())
            .field("requires_eula", &self.requires_eula())
            .field("autosave_on_exit", &self.autosave_on_exit())
            .field("uses_extended_banner", &self.uses_extended_banner())
            .field("requires_region_specific_game_rating", &self.requires_region_specific_game_rating())
            .field("uses_save_data", &self.uses_save_data())
            .field("records_usage", &self.records_usage())
            .field("disables_save_backups", &self.disables_save_backups())
            .field("new_3DS_exclusive", &self.new_3DS_exclusive())
            .finish()
    }
}

impl Display for EulaVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}.{}", self.major, self.minor)
    }
}
