use std::io::Cursor;

use byteorder::{ReadBytesExt, LE};

pub struct ApplicationTitle {
    pub short_description: [u16; 0x40],
    pub long_description: [u16; 0x80],
    pub publisher: [u16; 0x40],
}

#[derive(Default, Debug)]
pub struct ApplicationTitles ([ApplicationTitle; 16]);

impl ApplicationTitles {
    pub(super) fn parse_from_positioned_curser(reader: &mut Cursor<&[u8]>) -> Self {
        let mut titles: ApplicationTitles = ApplicationTitles::default();
        for title in titles.iter_mut() {
            for i in 0..0x40 {
                title.short_description[i] = reader.read_u16::<LE>().unwrap();
            }

            for i in 0..0x80 {
                title.long_description[i] = reader.read_u16::<LE>().unwrap();
            }

            for i in 0..0x40 {
                title.publisher[i] = reader.read_u16::<LE>().unwrap();
            }
        }

        titles
    }

    pub fn japanese(&self) -> &ApplicationTitle { &self.0[0] }
    pub fn english(&self) -> &ApplicationTitle { &self.0[1] }
    pub fn french(&self) -> &ApplicationTitle { &self.0[2] }
    pub fn german(&self) -> &ApplicationTitle { &self.0[3] }
    pub fn italian(&self) -> &ApplicationTitle { &self.0[4] }
    pub fn spanish(&self) -> &ApplicationTitle { &self.0[5] }
    pub fn chinese_simplified(&self) -> &ApplicationTitle { &self.0[6] }
    pub fn korean(&self) -> &ApplicationTitle { &self.0[7] }
    pub fn dutch(&self) -> &ApplicationTitle { &self.0[8] }
    pub fn portuguese(&self) -> &ApplicationTitle { &self.0[9] }
    pub fn russian(&self) -> &ApplicationTitle { &self.0[10] }
    pub fn chinese_traditional(&self) -> &ApplicationTitle { &self.0[11] }

    pub fn unused_1(&self) -> &ApplicationTitle { &self.0[12] }
    pub fn unused_2(&self) -> &ApplicationTitle { &self.0[13] }
    pub fn unused_3(&self) -> &ApplicationTitle { &self.0[14] }
    pub fn unused_4(&self) -> &ApplicationTitle { &self.0[15] }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<ApplicationTitle> {
        self.0.iter_mut()
    }
}

impl ApplicationTitle {
    pub fn short_description_string(&self) -> String {
        String::from_utf16_lossy(&self.short_description).trim_end_matches('\0').to_string()
    }

    pub fn long_description_string(&self) -> String {
        String::from_utf16_lossy(&self.long_description).trim_end_matches('\0').to_string()
    }

    pub fn publisher_string(&self) -> String {
        String::from_utf16_lossy(&self.publisher).trim_end_matches('\0').to_string()
    }
}

impl Default for ApplicationTitle {
    fn default() -> Self {
        Self { short_description: [0u16; 0x40], long_description: [0u16; 0x80], publisher: [0u16; 0x40] }
    }
}

impl std::fmt::Debug for ApplicationTitle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ApplicationTitle")
            .field("short_description", &self.short_description_string())
            .field("long_description", &self.long_description_string())
            .field("publisher", &self.publisher_string())
            .finish()
    }
}
