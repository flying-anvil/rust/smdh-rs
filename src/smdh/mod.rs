#![allow(unused)]

use std::{io::Cursor, path::Path};

use byteorder::{ReadBytesExt, LE};

use crate::prelude::{Result, Error};

mod application_title;
pub use application_title::*;

mod application_settings;
pub use application_settings::*;

mod icon_graphics;
pub use icon_graphics::*;

#[derive(Debug)]
pub struct SMDH {
    pub header: Header,
    pub application_titles: ApplicationTitles,
    pub application_settings: ApplicationSettings,
    pub reserved: [u8; 8],
    pub icon_graphics: IconGraphics,
}

#[derive(Debug)]
pub struct Header {
    pub magic: u32,
    pub version: u16,
    pub reserved: [u8; 2],
}

// ===== Methods ===============================================================

impl SMDH {
    pub const FILE_SIZE: usize = 14016;
    pub const MAGIC: [u8; 4] = [0x53, 0x4D, 0x44, 0x48]; // ASCII for "SMDH"

    pub fn try_from_file(path: &impl AsRef<Path>) -> Result<Self> {
        let bytes = std::fs::read(path).map_err(|error| Error::Generic(format!("{error}")))?;
        Self::try_from_bytes(&bytes)
    }

    pub fn from_bytes(bytes: &[u8]) -> Self {
        match Self::try_from_bytes(bytes) {
            Ok(value) => value,
            Err(error) => panic!("{error}"),
        }
    }

    pub fn try_from_bytes(bytes: &[u8]) -> Result<Self> {
        if bytes.len() != Self::FILE_SIZE {
            return Err(Error::SizeMismatch(Self::FILE_SIZE, bytes.len()));
        }

        let actual_magic = u32::from_be_bytes(bytes[0..4].try_into().unwrap());
        if bytes[0..4] != Self::MAGIC {
            return Err(Error::MagicMismatch(u32::from_be_bytes(Self::MAGIC), actual_magic));
        }

        let mut reader = Cursor::new(bytes);
        reader.set_position(0x04);

        let header = Header {
            magic: actual_magic,
            version: reader.read_u16::<LE>().unwrap(),
            reserved: reader.read_u16::<LE>().unwrap().to_le_bytes(),
        };

        let titles = ApplicationTitles::parse_from_positioned_curser(&mut reader);
        let settings = ApplicationSettings::parse_from_positioned_curser(&mut reader);
        let reserved = reader.read_u64::<LE>().unwrap().to_le_bytes();
        let icon_graphics = IconGraphics::read_from_positioned_curser(&mut reader);

        // println!("{header:#?}");
        // println!("{titles:#?}");
        // println!("{settings:#?}");

        Ok(SMDH {
            header,
            application_titles: titles,
            application_settings: settings,
            reserved,
            icon_graphics,
        })
    }
}
